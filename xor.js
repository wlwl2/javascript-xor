/*

x  y  x XOR y
-  -  -
0  0  0
1  0  1
0  1  1
1  1  0

Germundsson, Roger and Weisstein, Eric W. "XOR." From MathWorld--A Wolfram Web Resource. http://mathworld.wolfram.com/XOR.html
https://octave.sourceforge.io/octave/function/xor.html

*/

exports.xor = function (a, b) {
  if (a && !b) return true
  else if (!a && b) return true
  else return false
}

// Testing:

// function xor (a, b) {
//   if (a && !b) return 'a: ' + a + ' and b: ' + b + ' is ' + true
//   else if (!a && b) return 'a: ' + a + ' and b: ' + b + ' is ' + true
//   else return 'a: ' + a + ' and b: ' + b + ' is ' + false
// }
//
// console.log(xor(true, true))
// console.log(xor(false, true))
// console.log(xor(true, false))
// console.log(xor(false, false))
//
// // ?
// console.log(xor(1, true))
// console.log(xor(0, true))
// console.log(xor(-1, false))
// console.log(xor(null, false))
// console.log(xor(undefined, false))
// console.log(xor('test', false))
// console.log(xor('test', true))
